CONTENTS
--------
 * Introduction
 * Requirements
 * Installation


INTRODUCTION
------------
The hoverIntent Drupal module is a wrapper module for the hoverIntent jQuery
Plug-in.

hoverIntent is a plug-in that attempts to determine the user's intent... like a
crystal ball, only with mouse movement! It works like (and was derived from)
jQuery's built-in hover. However, instead of immediately calling the onMouseOver
function, it waits until the user's mouse slows down enough before making the
call.

Why? To delay or prevent the accidental firing of animations or ajax calls.
Simple timeouts work for small areas, but if your target area is large it may
execute regardless of intent. Also, because jQuery animations cannot be stopped
once they've started it's best not to start them prematurely.

This module does nothing on its own, it's just a wrapper for the JavaScript
library.


REQUIREMENTS
------------
This module requires both the Libraries API module and the hoverIntent jQuery
Plug-in.

The Libraries API module can be downloaded from
https://www.drupal.org/project/libraries. Make sure to use a current stable
version from the 7.x-2.x branch.

The hoverIntent jQuery Plug-in can be downloaded from
http://cherne.net/brian/resources/jquery.hoverIntent.html. Make sure to use the
minified version.


INSTALLATION
------------
Download and install the Libraries API module just as any other Drupal module.
Instructions can be found here:
https://www.drupal.org/docs/user_guide/en/extend-module-install.html

In order for the Libraries API and this module to find use the hoverIntent
jQuery Plug-in, it must be downloaded and placed in a hoverintent directory in
your site's sites/all/libraries directory. The name of the file must be
jquery.hoverIntent.minified.js.
